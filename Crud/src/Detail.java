import java.sql.*;

public class Detail {

    public static void main(String[] args) {
        ResultSet resultSet = null;
        PreparedStatement ps = null;

        try {

            // 1- load driver
            Class.forName("org.postgresql.Driver");

            // 2- connect to database
            Connection connection = DriverManager.getConnection(
                    "jdbc:postgresql://localhost:5432/postgres",
                    "postgres",
                    "Yuth12345");

            // 3- statement

            // get all data from table tb_customer
            String stQuery = "SELECT * FROM tb_student ORDER BY id";
            ps = connection.prepareStatement(stQuery);
            resultSet = ps.executeQuery();

            while (resultSet.next()) {
                System.out.println("ID : " + resultSet.getInt("id"));
                System.out.println("Name : " + resultSet.getString("name"));
                System.out.println("Gender : " + resultSet.getString("gender"));
                System.out.println("===========================");
            }



        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        } finally {
            // 4- Close connection
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }
    }
}
