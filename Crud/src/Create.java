import java.sql.*;
import java.util.Scanner;

public class Create {
    public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);

    try {
        Class.forName("org.postgresql.Driver");
        Connection conn = DriverManager.getConnection(
                "jdbc:postgresql://localhost:5432/postgres",
                "postgres",
                "Yuth12345");
        Statement st = conn.createStatement();
        System.out.println("Enter your ID: ");
        int stuId = scanner.nextInt();
        System.out.println("Enter your name: ");
        String stuName = scanner.next();
        System.out.println("Enter your gender: ");
        String stuGender = scanner.next();

        st.executeUpdate("insert into tb_student values ('"+stuId+"','"+stuName+"','"+stuGender+"')");

        System.out.println("Values inserted successful. Thank you!");

    } catch (ClassNotFoundException | SQLException e) {
        e.printStackTrace();
    }

    }
}
